import React, {useState, useEffect, useMemo} from 'react';
import { View, ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'; 
import { AuthContext } from './components/context';
import MainTabScreen from './screen/mainTabScreen';
import RootStackScreen from './screen/rootStackScreen';

export default function Index(){

    const [isLoading, setIsLoading] = useState(true);
    const [userToken, setUserToken] = useState(null);

    const authContext = useMemo(() => ({
        signIn: () => {
            setUserToken('abcde');
            setIsLoading(false);
        },
        signOut: () => {
            setUserToken(null);
            setIsLoading(false);
        },
        signUp: () => {
            setUserToken('abcde');
            setIsLoading(false)
        }
    }));

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 1000);
    }, []);

    if(isLoading){
        return(
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}> 
                <ActivityIndicator size="large" />
            </View>
        );
    }

    return(
        <AuthContext.Provider value={authContext}>
            <NavigationContainer>
                {
                    userToken !== null ? <MainTabScreen /> : <RootStackScreen />
                }    
            </NavigationContainer>
        </AuthContext.Provider>
    );
}