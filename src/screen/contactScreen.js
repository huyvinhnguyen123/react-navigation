import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput } from 'react-native';

// Split Components
import SearchForm from '../components/contactScreen/SearchForm';
import ChatContactActive from '../components/contactScreen/ChatContactActive';
import ChatContact from '../components/contactScreen/ChatContact';

export default function ContactScreen({navigation}){

    // Moving between screen when you split components
    const pressHandler = () => {
        navigation.navigate('Andrea Oralie');
    }

    return(
        <View style={styles.container}>
            <ScrollView style={{ padding: 5 }}>

                {/*--Component SearchForm--*/}
                    <SearchForm />
                {/*------------------------*/}

                <Text style={{ fontSize: 20, fontStyle: "bold", color: "#b1b1b1" }}> A </Text>

                {/*--Component ChatContactActive--*/}
                    <ChatContactActive pressHandler={pressHandler} />
                {/*-------------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

                <Text style={{ fontSize: 20, fontStyle: "bold", color: "#b1b1b1" }}> B </Text>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

                <Text style={{ fontSize: 20, fontStyle: "bold", color: "#b1b1b1" }}> C </Text>

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ChatContact--*/}
                    <ChatContact />
                {/*-------------------------*/}

            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderRadius: 3
    },

    searchSection: {
        flex: 1,
        margin: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5',
        borderRadius: 3,
    },

    searchIcon: {
        padding: 10,
    },

    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        color: '#424242',
    },
});