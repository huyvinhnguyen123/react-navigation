import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button, Platform, Image, ScrollView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function EditProfileScreen({navigation}){
    
    const [data, setData] = useState({
      email:"",
      phone:"",
      address:"",
      username:""
    })

    const handelChangeText = (val) => {
      return(
        setData({
          ...data,

        })
      )
    }
  
    return(
        <View style={styles.container}>
        
        <ScrollView>
            <View style ={styles.userInfoSection}>
            <View style={{ marginTop: 50, alignItems: 'center'}}>
              <TouchableOpacity>
                <Image style={{ height: 100, width: 100, marginRight: 20, borderRadius: 50 }} source = {require('../../image/user.jpg')} />
                <Icon style={{marginLeft: 40, marginRight: 60}} name="camera" color="#777777" size={20} />
              </TouchableOpacity>
            </View>
            </View>

            <View style = {styles.userInfoSection}>
            <Text style={styles.text_edit}>Username</Text>
            <View style={styles.action}>
                <Icon name="account" color="#777777" size={20}/>
                <TextInput style={styles.textInput} onChangeText={(val) => handelChangeText(val)} />
            </View>
            <Text style={styles.text_edit}>Status</Text>
            <View style={styles.action}>
                <Icon name="comment" color="#777777" size={20}/>
                <TextInput style={styles.textInput} onChangeText={(val) => handelChangeText(val)} />
            </View>
            <Text style={styles.text_edit}>Address</Text>
            <View style={styles.action}>
                <Icon name="map-marker-radius" color="#777777" size={20}/>
                <TextInput style={styles.textInput} onChangeText={(val) => handelChangeText(val)} />
            </View>
            <Text style={styles.text_edit}>Phone-number</Text>
            <View style={styles.action}>
                <Icon name="phone" color="#777777" size={20}/>
                <TextInput style={styles.textInput} onChangeText="+91-900000009" />
            </View>
            <Text style={styles.text_edit}>Email</Text>
            <View style={styles.action}>
                <Icon name="email" color="#777777" size={20}/>
                <TextInput style={styles.textInput} onChangeText="Andrea_Oralie@email.com" />
            </View>
            </View>

            <View style={styles.button}>
              <TouchableOpacity>
                <View style={styles.signIn}>
                  <Text style={[styles.textSign,{color:'#fff'}]}> Save Changes </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.signIn,{backgroundColor: '#fff', borderColor: '#34cfeb', borderWidth: 1, marginTop: 15}]} onPress={() => navigation.goBack()}>
                <Text style={[styles.textSign,{color: '#34cfeb'}]}> Cancel </Text>
              </TouchableOpacity>
            </View>
              
        </ScrollView>
      </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
      },
      userInfoSection: {
        paddingHorizontal: 30,
        marginBottom: 25,
      },
      action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
      },
      textInput: {
        flex: 1,
        marginTop: Platform.And === 'ios' ? 0 : -2,
        paddingLeft: 10,
        color: '#05375a',
      },
      text_edit: {
        color: '#05375a',
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 20
      },
      button: {
        alignItems: 'center',
        marginBottom: 10
      },
      signIn: {
        width: 300,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#34cfeb",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#34cfeb',
        marginTop: 10,
      },
});