import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import  Icon from 'react-native-vector-icons/Ionicons';
import HomeScreen from './homeScreen';
import ContactScreen from './contactScreen';
import ProfileScreen from './profileScreen';
import ChatScreen from './chatScreen';
import EditProfileScreen from './editProfileScreen';
import EditPassword from './editPassword';
import SupportScreen from './supportScreen';
import GroupScreen from './groupScreen';

const Tab = createBottomTabNavigator();

const HomeStack = createStackNavigator();
const ContactStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const HomeStackScreen = () => (
    <HomeStack.Navigator screenOptions = {{
        headerStyle:{
            backgroundColor:'#34cfeb',
        },
        headerTitleAlign: 'center',
        headerTintColor:'#fff',
        headerTitleStyle:{
            fontWeight:'bold',
        }
    }}>

        <HomeStack.Screen name="Home" component={HomeScreen} options={{
            title: "Messages",
            // headerLeft: () => (
            //   <Icon.Button name="ios-menu" size={25} backgroundColor="#009387" 
            //   onPress={() => navigation.openDrawer()}></Icon.Button>
            // )
          }}
        />

        <HomeStack.Screen name="Andrea Oralie" component={ChatScreen}/>

    </HomeStack.Navigator>
)

const ContactStackScreen = () => {
    return(
        <ContactStack.Navigator screenOptions = {{
            headerStyle:{
                backgroundColor:'#34cfeb',
            },
            headerTitleAlign: 'center',
            headerTintColor:'#fff',
            headerTitleStyle:{
                fontWeight:'bold',
            }
        }}>
            <ContactStack.Screen name="Contact" component={ContactScreen} />
            <ContactStack.Screen name="Andrea Oralie" component={ChatScreen} />
        </ContactStack.Navigator>
    )
}

const ProfileStackScreen = () => {
    return(
        <ProfileStack.Navigator headerMode='none'>
            <ProfileStack.Screen name="Profile" component={ProfileScreen}/>
            <ProfileStack.Screen name="EditProfile" component={EditProfileScreen}/>
            <ProfileStack.Screen name="EditPassword" component={EditPassword}/>
            <ProfileStack.Screen name="CreateGroup" component={GroupScreen}/>
            <ProfileStack.Screen name="Support" component={SupportScreen}/>
        </ProfileStack.Navigator>
    )
}

export default function MainTabScreen(){
    return(
        <Tab.Navigator screenOptions = {({ route }) => ({
            tabBarIcon: ({ focused, color, size }) =>  {
                let iconName;
                if(route.name === 'Messages'){
                    iconName = focused ? 'chatbubble-ellipses' : 'chatbubble-ellipses-outline'
                } 
                else if(route.name === 'Contacts'){
                    iconName = focused ? 'people' : 'people-outline'
                } 
                else if(route.name === 'Profile'){
                    iconName = focused ? 'person' : 'person-outline'
                }
                return <Icon name={iconName} size={size} color={color} />;
            } 
        })}
        tabBarOptions={{
            activeTintColor: '#34cfeb',
            inactiveTintColor: 'gray',
        }}>


            <Tab.Screen name="Messages" component={HomeStackScreen} /> 

            <Tab.Screen name="Contacts" component={ContactStackScreen} />

            {/* {() => (
                    <ContactStack.Navigator>
                        <ContactStack.Screen
                            name="Contacts"
                            component={ContactScreen}
                            options={{
                                headerTitleAlign: 'center',
                                headerTintColor:'#fff',
                                headerStyle: {
                                    // backgroundColor: '#eb865b',
                                    backgroundColor: '#34cfeb',
                                },
                                // headerTitleAlign: 'center',
                            }}
                        />
                    </ContactStack.Navigator>
                )} */}


            <Tab.Screen name="Profile" component={ProfileStackScreen} />
            
        </Tab.Navigator>
    );
}

