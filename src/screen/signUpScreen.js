import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, Platform, StyleSheet, ImageBackground, Alert } from 'react-native';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import BGimage from '../../assets/office2.jpg';

//Split Components
import EmailTextInput from '../components/signUpScreen/EmailTextInput';
import PasswordTextInput from '../components/signUpScreen/PasswordTextInput';
import ConfirmPasswordTextInput from '../components/signUpScreen/ConfirmPasswordTextInput';
import SignInButton from '../components/signUpScreen/SignInButton';

export default function SignInScreen ({ navigation }){
    
    // Moving between screen when you split components
    const pressHandler = () => {
      navigation.navigate('SignInScreen');
    }

    return(
        <ImageBackground source={BGimage} style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.text_header}>Register Account</Text>
            </View> 
        <Animatable.View animation="fadeInUpBig" style={styles.footer}>
          <Text style={styles.text_footer}> Email </Text>

          {/*--Component EmailTextInput--*/}
            <EmailTextInput />
          {/*----------------------------*/}

          <Text style={[styles.text_footer, {marginTop: 20}]}> Password </Text>

          {/*--Component PasswordTextInput--*/}
            <PasswordTextInput />
          {/*-------------------------------*/}

          <Text style={[styles.text_footer, {marginTop: 20}]}> Confirm Password </Text>

          {/*--Component ConfirmPasswordTextInput--*/}
            <ConfirmPasswordTextInput />
          {/*--------------------------------------*/}

          <View style={styles.button}>
            
            {/*--Component SignInButton--*/}
              <SignInButton pressHandler={pressHandler} />
            {/*-------------------------------*/}

          </View>

        </Animatable.View> 
      </ImageBackground>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#009387'
  },
  header: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
      textAlign: 'center',
      paddingHorizontal: 20,
      paddingBottom: 50,
  },
  footer: {
      flex: 3,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingHorizontal: 20,
      paddingVertical: 30
  },
  text_header: {
      color: '#fff',
      fontWeight: 'bold',
      fontSize: 25
  },
  text_footer: {
      color: '#05375a',
      fontSize: 18
  },
  actionError: {
      flexDirection: 'row',
      marginTop: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#FF0000',
      paddingBottom: 5
  },
  errorMsg: {
      color: '#FF0000',
      fontSize: 14,
  },
  button: {
      alignItems: 'center',
      marginTop: 50
  },
});
  