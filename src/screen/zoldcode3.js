import React from 'react';
import { useEffect } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function ProfileScreen({route, navigation}){

    useEffect(() => {
        if (route.params?.post) {
          // Post updated, do something with `route.params.post`
          // For example, send the post to the server
        }
      }, [route.params?.post]);

    return(
        <View style={styles.container}>
            <Text>Profile Screen</Text>
            <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
            <Text style={{ margin: 10 }}>Post: {route.params?.post}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    }
})