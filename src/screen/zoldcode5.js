import React, { useState, useCallback, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Bubble, GiftedChat, Send } from 'react-native-gifted-chat';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default function ChatScreen(){
    const [ messages, setMessages ] = useState([]);

    useEffect(() =>{
        setMessages([
            {
                _id: 1,
                text: 'Hello my friend!!!',
                createdAt: new Date(),
                // system: true,
                user: {
                    _id: 2,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
                image: 'https://placeimg.com/140/140/any'
            },

            {
                _id: 2,
                text: 'Hello i\'m Andrea Oralie',
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 3,
                text: 'I\'m 23 years old',
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 4,
                text: 'Hello i\'m Ashaxi',
                createdAt: new Date(),
                user: {
                    _id: 1,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 5,
                text: 'I\'m 23 years old',
                createdAt: new Date(),
                user: {
                    _id: 1,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 6,
                text: 'Nice, what is your job?',
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 7,
                text: 'I\'m App Developer',
                createdAt: new Date(),
                user: {
                    _id: 1,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 8,
                text: 'Oh, me too!',
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 9,
                text: 'It\'s good to see you',
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
            {
                _id: 10,
                text: 'Me too!',
                createdAt: new Date(),
                user: {
                    _id: 1,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
        ])
    }, [])

    const onSend = useCallback((messages = []) => {
        setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
    }, [])

    const renderSend = (props) => {
        return(
            <Send {...props}>
                <View>
                    <Icon name="send-circle" style={{marginBottom: 5, marginRight: 5}} size={32} color="#34cfeb" />
                </View>
            </Send>
        );
    }

    const renderBubble = (props) => {
        return(
            <Bubble
                {...props}
                wrapperStyle={{
                    right: {
                        backgroundColor: '#34cfeb',
                    },

                    left:{
                        backgroundColor: 'grey',
                    },
                }}
                textStyle={{
                    right: {
                        color: '#fff',
                    },

                    left: {
                        color: '#fff',
                    },
                }}
            />
        );
    }

    const scrollToBottomComponent = () => {
        return(
          <FontAwesome name='angle-double-down' size={22} color='#333' />
        );
    }

    return(
        <GiftedChat 
            messages={messages}
            onSend={messages => onSend(messages)}
            user = {{
                _id: 1,
            }}
            renderBubble = {renderBubble}
            alwaysShowSend
            renderSend = {renderSend}
            scrollToBottom
            scrollToBottomComponent={scrollToBottomComponent}
        />
    )
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});