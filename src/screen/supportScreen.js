import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import BGimage from '../../assets/support.png';
import * as Animatable from 'react-native-animatable';

export default function SupportScreen({navigation}){
    return(
        <View style={styles.container}>
            <ScrollView>
                <View style={[styles.header,{marginTop: 60}]}>
                    <Image source={BGimage} style={{width: 175, height: 160}}>
                    </Image>
                </View>
                <View style={styles.footer}>
                    <Animatable.View animation="fadeInUp" duration={600}>
                        <Text style={styles.title}> How can we help you? </Text>
                    </Animatable.View>
                    <Animatable.View animation="fadeInLeft" duration={700}>
                        <Text style={styles.text}> It looks like you are experiencing problems with our sign up process. We are here to help so please get in touch with us </Text>
                    </Animatable.View>
                    <Animatable.View style={{flexDirection: 'row'}} animation="fadeInLeft" duration={700}>
                        <TouchableOpacity style={styles.cardButton}>
                            <FontAwesome name="comment" color="#05375a" size={20}/>
                            <Text style={{color: '#05375a', fontWeight: '500'}}> Chat Us </Text>
                        </TouchableOpacity>
                        <View style={{ margin: 10 }}></View>
                        <TouchableOpacity style={styles.cardButton}>
                            <FontAwesome name="envelope" color="#05375a" size={20}/>
                            <Text style={{color: '#05375a', fontWeight: '500'}}> Email Us </Text>
                        </TouchableOpacity>
                    </Animatable.View>
                    <Animatable.View style={styles.button} animation="fadeInUp" duration={600}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <View style={styles.backButton}>
                                <Text style={{color:'#fff', fontSize: 17, fontWeight: 'bold'}}> Back </Text>
                            </View>
                        </TouchableOpacity>
                    </Animatable.View>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        flex: 1,
        backgroundColor: '#fff',
        // borderTopLeftRadius: 30,
        // borderTopRightRadius: 30,
        paddingVertical: 20,
        paddingHorizontal: 25,
    },
      title: {
      color: '#05375a',
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    text: {
        color: 'grey',
        marginTop: 10,
        fontWeight: '500',
        textAlign: 'center'
    },
    button: {
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    cardButton: {
        width: 150,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#05375a',
        marginTop: 30
    },
    backButton: {
        width: 300,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#34cfeb",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#34cfeb',
    }
})