import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Image, TextInput } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';

// Split Components
import HeaderTitle from '../components/groupScreen/HeaderTitle';
import NameTextInput from '../components/groupScreen/NameTextInput';
import SearchForm from '../components/groupScreen/SearchForm';
import ContactSub from '../components/groupScreen/ContactSub';
import ContactAdd from '../components/groupScreen/ContactAdd';

export default function GroupScreen({navigation}){

    // Moving between screen when you split components
    const pressHandler = () => {
        navigation.goBack();
    }

    return(
        <View style={styles.container}>
            <ScrollView style={{ padding: 5 }}>

                {/*--Component HeaderTitle--*/}
                    <HeaderTitle pressHandler={pressHandler} />
                {/*-------------------------*/}

                {/*--Component NameTextInput--*/}
                    <NameTextInput />
                {/*---------------------------*/}

                {/*--Component SearchForm--*/}
                    <SearchForm />
                {/*------------------------*/}

                {/*--Component ContactSubtract--*/}
                    <ContactSub />
                {/*-----------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ContactSubtract--*/}
                    <ContactSub />
                {/*-----------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#333' }}></View>

                <Text style={{ fontSize: 20, fontStyle: "bold", color: "#b1b1b1" }}> A </Text>
                
                {/*--Component ContactAdd--*/}
                    <ContactAdd />
                {/*------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ContactAdd--*/}
                    <ContactAdd />
                {/*------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ContactAdd--*/}
                    <ContactAdd />
                {/*------------------------*/}

                <Text style={{ fontSize: 20, fontStyle: "bold", color: "#b1b1b1" }}> B </Text>
                
                {/*--Component ContactAdd--*/}
                    <ContactAdd />
                {/*------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ContactAdd--*/}
                    <ContactAdd />
                {/*------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

                {/*--Component ContactAdd--*/}
                    <ContactAdd />
                {/*------------------------*/}

                <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
});