import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';


export default function DetailScreen({route, navigation}){

    const { itemID, itemName, price } = route.params;

    return(
        <View style={styles.container}>
            <Text>Detail Screen</Text>
            <Text>itemID: {JSON.stringify(itemID)}</Text>
            <Text>itemName: {JSON.stringify(itemName)}</Text>
            <Text>price: {JSON.stringify(price)}</Text>
            <Button
                title="Go to details"
                onPress={() => navigation.push('Details',{
                    itemID: Math.floor(Math.random() * 100),
                })}
            />
            <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
            <Button title="Go back" onPress={() => navigation.goBack()} />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    }
})