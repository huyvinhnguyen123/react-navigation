import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './screen/homeScreen';
import ContactScreen from './screen/contactScreen';
import ProfileScreen from './screen/profileScreen';
import ChatScreen from './screen/chatScreen';

const Tab = createBottomTabNavigator();
const ContactStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const HomeStack = createStackNavigator();

// const HomeStack = ( navigation ) => {
//     <Stack.Navigator>
//         <Stack.Screen name="Messengers" 
//         component={HomeScreen} 
//         options={{
//             headerTintColor: "#fff",
//             headerStyle: {
//                 backgroundColor: 'royalblue',
//             },
//             headerTitleAlign: 'center',
//         }}/>
//     </Stack.Navigator>
// };

function HomeStackScreen(){
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={HomeScreen} />
      <HomeStack.Screen name="Chat" component={ChatScreen} />
    </HomeStack.Navigator>
}

export default function Index(){
    return(
        <NavigationContainer>
            <Tab.Navigator screenOptions = {({ route }) => ({
                tabBarIcon: ({ focused, color, size }) =>  {
                    let iconName;
                    if(route.name === 'Messengers'){
                        iconName = focused ? 'chatbubble-ellipses' : 'chatbubble-ellipses-outline'
                    } 
                    else if(route.name === 'Contacts'){
                        iconName = focused ? 'people' : 'people-outline'
                    } 
                    else if(route.name === 'Profile'){
                        iconName = focused ? 'person' : 'person-outline'
                    }
                    return <Icon name={iconName} size={size} color={color} />;
                } 
            })}
            tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'gray',
            }}>
                <Tab.Screen name="Home" component={HomeStackScreen} /> 

                    {/* {() => (
                        <HomeStack.Navigator>
                            <HomeStack.Screen
                                name="Messengers"
                                component={HomeScreen}
                                options={{
                                    headerTintColor: "#000",
                                    headerStyle: {
                                        // backgroundColor: '#eb865b',
                                        backgroundColor: '#fff',
                                    },
                                    // headerTitleAlign: 'center',
                                }}
                            />
                            <HomeStack.Screen name="Profile" component={ProfileScreen} />
                        </HomeStack.Navigator>
                    )} */}


                <Tab.Screen name="Contacts">
                {() => (
                        <ContactStack.Navigator>
                            <ContactStack.Screen
                                name="Contacts"
                                component={ContactScreen}
                                options={{
                                    headerTintColor: "#000",
                                    headerStyle: {
                                        // backgroundColor: '#eb865b',
                                        backgroundColor: '#fff',
                                    },
                                    // headerTitleAlign: 'center',
                                }}
                            />
                            <ContactStack.Screen name="Profile" component={ProfileScreen} />
                        </ContactStack.Navigator>
                    )}
                </Tab.Screen>

                <Tab.Screen name="Profile" component={ProfileScreen} />

            </Tab.Navigator>
        </NavigationContainer>
    );
}