import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';

// Split Components
import SearchForm from '../components/homeScreen/SearchForm';
import ChatContactActive from '../components/homeScreen/ChatContactActive';
import ChatContact from '../components/homeScreen/ChatContact';

export default function HomeScreen({navigation}){

    // Moving between screen when you split components
    const pressHandler = () => {
        navigation.navigate('Andrea Oralie');
    }

    return(
        <View style={styles.container}>
            <ScrollView style={{ padding: 5 }}>

            {/*--Component SearchForm--*/}
                <SearchForm />
            {/*------------------------*/}

            {/*--Component ChatContactActive--*/}
                <ChatContactActive pressHandler={pressHandler} />
            {/*-------------------------------*/}  

            <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            {/*--Component ChatContact--*/}
                <ChatContact />
            {/*-------------------------*/}

            <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            {/*--Component ChatContact--*/}
                <ChatContact />
            {/*-------------------------*/}   

            <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            {/*--Component ChatContact--*/}
               <ChatContact />
            {/*-------------------------*/} 

            <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            {/*--Component ChatContact--*/}
                <ChatContact />
            {/*-------------------------*/}

            <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            {/*--Component ChatContact--*/}
                <ChatContact />
            {/*-------------------------*/}

            <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            {/*--Component ChatContact--*/}
                <ChatContact />
            {/*-------------------------*/}

            <View style = {{ margin: 10, borderWidth: 0.5, borderColor: '#f5f5f5' }}></View>

            {/*--Component ChatContact--*/}
                <ChatContact />
            {/*-------------------------*/}

            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:'#fff'
    }
});