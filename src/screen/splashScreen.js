import React from 'react';
import { View, Text, TouchableOpacity, Dimensions, StyleSheet, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useTheme } from '@react-navigation/native';
import BGimage from '../../assets/office.jpg';

export default function SplashScreen ({ navigation }){
  
  const { colors } = useTheme();

  return (
    <View style={styles.container}>
      <ImageBackground source={BGimage} style={styles.header}>
        <Animatable.Image
          animation="bounceIn"
          style={[styles.logo,{width: 145, height: 130}]}
          resizeMode="stretch"
        />
      </ImageBackground>
      <View style={styles.footer}>
        <Animatable.View animation="fadeInUp" duration={600}>
          <Text style={styles.title}> Stay connected with everyone! </Text>
          <Text style={styles.text}> Sign in with account </Text>
          <View style={styles.button}>
            <TouchableOpacity onPress={() => navigation.navigate('SignInScreen')}>
              <View style={styles.signIn}>
                <Text style={styles.textSign}> Get Started </Text>
                <MaterialIcons 
                  name="navigate-next"
                  color="#fff"
                  size={20}
                />
              </View>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    </View>
  );
}

const {height} = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#333'
  },
  header: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      // borderTopLeftRadius: 30,
      // borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  logo: {
      width: height_logo,
      height: height_logo
  },
  title: {
      color: '#05375a',
      fontSize: 30,
      fontWeight: 'bold',
      textAlign: 'center'
  },
  text: {
      color: 'grey',
      marginTop: 5,
      textAlign: 'center'
  },
  button: {
      alignItems: 'center',
      marginTop: 30
  },
  signIn: {
      width: 150,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50,
      flexDirection: 'row',
      backgroundColor: "#34cfeb",
      // borderRadius: 50,
      // borderWidth: 1,
      // borderColor: '#009387',
      // marginTop: 10,
  },
  textSign: {
      color: 'white',
      fontWeight: 'bold'
  }
});
  