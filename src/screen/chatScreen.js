import React, { useState, useCallback, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Image, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { color } from 'react-native-reanimated';

export default function ChatScreen(){
   return(
        <View style={styles.container}>
            <ScrollView style={{ padding: 5, marginTop: 10 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 10, fontWeight: '500', color: '#b1b1b1' }}>
                    <Text>Sunday, 19 January 2025 </Text>
                </View>
                <View style = {{ flexDirection: 'column', padding: 5 }}>
                    <View style = {{ flexDirection: 'row' }}>
                        <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../image/user.jpg')} />
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingRight: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}> Hello i'm Andrea Oralie, Can i meet you at 7:00 pm at Coffee House? </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5}}>
                    <View style = {{ flexDirection: 'row', justifyContent:'flex-end'}}>
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingBottom: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}>  Okay, I will see you at Coffee House, please to meet you! </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5 }}>
                    <View style = {{ flexDirection: 'row' }}>
                        <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../image/user.jpg')} />
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingRight: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}> I'm 22 years old. How old are you? </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5}}>
                    <View style = {{ flexDirection: 'row', justifyContent:'flex-end'}}>
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingBottom: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}>  I'm 22 years old too. </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5 }}>
                    <View style = {{ flexDirection: 'row' }}>
                        <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../image/user.jpg')} />
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingRight: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}> Nice to meet you! </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5}}>
                    <View style = {{ flexDirection: 'row', justifyContent:'flex-end'}}>
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingBottom: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}>  Nice to meet you too! </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5 }}>
                    <View style = {{ flexDirection: 'row' }}>
                        <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../image/user.jpg')} />
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingRight: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}> Do you often go to swim? </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5}}>
                    <View style = {{ flexDirection: 'row', justifyContent:'flex-end'}}>
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingBottom: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}>  Yes i have, i always go to swim on weekends  </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5 }}>
                    <View style = {{ flexDirection: 'row' }}>
                        <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../image/user.jpg')} />
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingRight: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}> Great! me too! </Text>
                        </View>
                    </View>
                </View>

                <View style = {{ flexDirection: 'column', padding: 5}}>
                    <View style = {{ flexDirection: 'row', justifyContent:'flex-end'}}>
                        <View style={{ flexDirection: 'column', width: '55%', backgroundColor: 'grey', borderRadius: 3, paddingLeft: 5, paddingBottom: 5 }}>
                            <Text style={{ color:'#fff', fontWeight: '500', paddingTop: 3 }}>  Okay, I will see you at Coffee House, please to meet you! </Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.input}>
                <TouchableOpacity style={{ padding: 5 }}>
                    <FontAwesome style={{paddingTop: 2}} name="paperclip" color="grey" />
                </TouchableOpacity>
                <View style={{flexDirection: 'row', backgroundColor: '#f5f5f5', padding: 5, width: '95%'}}>
                    <TextInput style={{ width: '100%' }} placeholder="Input some text..." />
                    <TouchableOpacity style={{marginLeft: 'auto'}}>
                        <FontAwesome style={{paddingTop: 2}} name="search" color="grey"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 5}}>
                        <FontAwesome style={{paddingTop: 2}} name="image" color="grey" size="25"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 5}}>
                        <FontAwesome style={{paddingTop: 2}} name="smile-o" color="grey" size="25"/>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
   );
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },

    input: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor:"#fff"
    },

    
});