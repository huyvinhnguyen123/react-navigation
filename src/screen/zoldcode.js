import React, {useState} from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export default function HomeScreen({navigation}){

    const [postText, setPostText] = useState([
        {itemID: 1, itemName: 'Laptop', price: 200 + '$'},
        {itemID: 2, itemName: 'Mobile', price: 100 + '$'}
    ]);

    const [count, setCount] = React.useState(0);

    React.useLayoutEffect(() => {
        navigation.setOptions({
        headerRight: () => (
            <Button onPress={() => setCount(c => c + 1)} title="Update count" />
        ),
        });
    }, [navigation, setCount]);

    return(
        
        <View style={styles.container}>
            <Text>Home Screen</Text>
            <Text>Count: {count}</Text>
            {/* {
                postText.map((item) => {
                    return(
                        <Text>{item.price}</Text>
                    )
                })
            } */}


            <TextInput
                multiline
                placeholder="What's on your mind?"
                style={{ height: 200, padding: 10, backgroundColor: 'white' }}
                value={postText.map((x)=>x.price)}
                onChangeText={setPostText}
            />
            <Button
                title="Go to details"
                onPress={() => navigation.navigate('Details', {
                    name: 'Custom Detail',
                    itemID: 1,
                    itemName: 'Laptop',
                    price: 200 + '$'
                })}
            />
             <Button
                title="Go to profile"
                onPress={() => navigation.navigate('Profile', {
                    name: 'Profile',
                    params: { post: postText },
                    merge: true,
                })}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    }
})