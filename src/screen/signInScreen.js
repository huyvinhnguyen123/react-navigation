import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import BGimage from '../../assets/office2.jpg';

// Split Components 
import EmailTextInput from '../components/signInScreen/EmailTextInput';
import PasswordTextInput from '../components/signInScreen/PasswordTextInput';
import SignInButton from '../components/signInScreen/SignInButton';
import SignUpButton from '../components/signInScreen/SignUpButton';

export default function SignInScreen({navigation}) {
    
  // Moving between screen when you split components
  const pressHandler = () => {
    navigation.navigate('SignUpScreen')
  }

  return(
    <ImageBackground source={BGimage} style={styles.container}>
      <View style={styles.header}>
        <Text style={[styles.text_header,{color: '#fff'}]}>Welcome, Join With Us!</Text>
      </View> 

      {/* This view is assigned animation */}
      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <Text style={styles.text_footer}> Email </Text>
        
        {/*--Component EmailTextInput--*/}
          <EmailTextInput />
        {/*----------------------------*/}
        
        <Text style={[styles.text_footer, {marginTop: 20}]}> Password </Text>
        
        {/*--Component PasswordTextInput--*/}
          <PasswordTextInput />
        {/*-------------------------------*/}

        <TouchableOpacity>
          <Text style={{color: '#34cfeb', marginTop:15}}> Forgot Password? </Text>
        </TouchableOpacity>

        <View style={styles.button}>

        {/*--Component SignInButton--*/}
          <SignInButton />
        {/*-------------------------------*/}

        {/*--Component SignUpButton--*/}
          <SignUpButton pressHandler={pressHandler} />
        {/*-------------------------------*/}

        </View>

      </Animatable.View> 
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#34cfeb'
  },
  header: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
      textAlign: 'center',
      paddingHorizontal: 20,
      paddingBottom: 50,
  },
  footer: {
      flex: 3,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingHorizontal: 20,
      paddingVertical: 30
  },
  text_header: {
      color: '#fff',
      fontWeight: 'bold',
      fontSize: 25
  },
  text_footer: {
      color: '#05375a',
      fontSize: 18
  },
  actionError: {
      flexDirection: 'row',
      marginTop: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#FF0000',
      paddingBottom: 5
  },
  errorMsg: {
      color: '#FF0000',
      fontSize: 14,
  },
  button: {
      alignItems: 'center',
      marginTop: 50
  }
});
  