import React from 'react';
import { StyleSheet, Button, View, Text } from 'react-native';


export default function HomeScreen({navigation}){
    return(
        <View style={styles.container}>
            <Button
                title="Go to details"
                onPress={() => navigation.navigate('Details', {
                    name: 'Custom Detail',
                    itemID: 1,
                    itemName: 'Laptop',
                    price: 200 + '$'
                })}
            />

            <Button
                title="Go to profile"
                onPress={() => navigation.navigate('Profile')}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    }
})
