import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';

// Split Components
import UserInfo from '../components/profileScreen/UserInfo';
import InfoBox from '../components/profileScreen/InfoBox';
import Menu from '../components/profileScreen/Menu';

export default function ProfileScreen({ navigation }){

    // Moving between screen when you split components
    const pressHandler = (screen) => {
      switch(screen){
        case 'EditProfile':
          navigation.navigate('EditProfile');
          break;
        case 'EditPassword':
          navigation.navigate('EditPassword');
          break;
        case 'CreateGroup':
          navigation.navigate('CreateGroup');
          break;
        case 'Support':
          navigation.navigate('Support');
          break;
        default:
          null;
          console.log("Screen not found!");
          break;
      }
    }
      
    return(
        <View style={styles.container}>
        <ScrollView>

            {/*--Component UserInfo--*/}
              <UserInfo />
            {/*----------------------*/}

            {/*--Component InfoBox--*/}
              <InfoBox />
            {/*---------------------*/}

            {/*--Component Menu--*/}
              <Menu pressHandler={pressHandler} />
            {/*------------------*/}

        </ScrollView>
      </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    }
});