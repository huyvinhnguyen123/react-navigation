import React, { useState } from 'react';
import { StyleSheet, Platform, TextInput, TouchableOpacity, Text, View } from 'react-native';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

export default function EmailTextInput() {

    const [data, setData] = useState({
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
    })

    const handelPasswordChange = (val) => {
        if(val.trim().length >= 6){
          setData({
            ...data,
            password: val,
            isValidPassword: true
          })
        } else{
          setData({
            ...data,
            password: val,
            isValidPassword: false
          })
        }
    }

    const updateSecurityEntry = () => {
        setData({
          ...data,
          secureTextEntry: !data.secureTextEntry
        })
    }

    return (
        <View>
            <View style={styles.action}>
                <FontAwesome
                    name="lock"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Your Password"
                    secureTextEntry={data.secureTextEntry ? true : false}
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={(val) => handelPasswordChange(val)}
                />

                <TouchableOpacity onPress={updateSecurityEntry}>
                    { data.secureTextEntry ? 
                    <Feather
                        name="eye-off"
                        color="grey"
                        size={20}
                    /> :
                    <Feather
                        name="eye"
                        color="grey"
                        size={20}
                    />
                    }
                </TouchableOpacity>
            </View>

            {
                data.isValidPassword ? null :
                <Animatable.View animation="fadeInLeft" duration={600}>
                    <Text style={styles.errorMsg}> Password must be 6 characters long. </Text>
                </Animatable.View>
            }

        </View>
    );
}

const styles = StyleSheet.create({
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },

    textInput: {
        flex: 1,
        marginTop: Platform.And === 'ios' ? 0 : -2,
        paddingLeft: 10,
        color: '#05375a',
    },
})
