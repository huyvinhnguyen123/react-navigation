import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export default function EmailTextInput({pressHandler}) {

    return (

        <TouchableOpacity 
            style={[styles.signIn,{backgroundColor: '#fff', borderColor: '#34cfeb', borderWidth: 1, marginTop: 15}]} 
            onPress={() => pressHandler()}
        >
            <Text style={[styles.textSign,{color: '#34cfeb'}]}> Sign In </Text>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({
    signIn: {
        width: 300,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#34cfeb",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#34cfeb',
        marginTop: 10,
    },
    textSign: {
        fontSize: 17,
        fontWeight: 'bold'
    }
})
