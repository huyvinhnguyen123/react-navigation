import React from 'react'
import { Text, View, Image } from 'react-native';

export default function ChatContact() {
    return (
        <View style = {{ flexDirection: 'column', padding: 5 }}>
            <View style = {{ flexDirection: 'row' }}>
                <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../../image/user2.jpg')} />
                    <View style={{ flexDirection: 'column', width: '69%', marginTop: 15 }}>
                        <Text style={{ fontWeight: 'bold', }}> Andrea Oralie </Text>
                    </View>
            </View>
        </View>
    )
}
