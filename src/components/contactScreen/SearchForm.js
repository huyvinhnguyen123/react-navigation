import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default function SearchForm() {
    return (
        <View style={styles.searchSection}>
            <Icon style={styles.searchIcon} name="search" size={20} color="#000"/>
                <TextInput
                    style={styles.input}
                    placeholder="Search..."
                    underlineColorAndroid="transparent"
                />
        </View>
    )
}

const styles = StyleSheet.create({

    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderRadius: 3
    },

    searchSection: {
        flex: 1,
        margin: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5',
        borderRadius: 3,
    },

    searchIcon: {
        padding: 10,
    },

    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        color: '#424242',
    },
});
