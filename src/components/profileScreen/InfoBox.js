import React from 'react'
import { StyleSheet, View } from 'react-native';
import { Title, Caption } from 'react-native-paper';

export default function InfoBox() {
    return (
        <View style={styles.infoBoxWrapper}>
            <View style={[styles.infoBox, {borderRightColor: '#dddddd', borderRightWidth: 1}]}>
                <Title>3</Title>
                <Caption>Groups</Caption>
            </View>
            <View style={styles.infoBox}>
                <Title>17</Title>
                <Caption>Friends</Caption>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
      infoBoxWrapper: {
        borderBottomColor: '#dddddd',
        borderBottomWidth: 1,
        borderTopColor: '#dddddd',
        borderTopWidth: 1,
        flexDirection: 'row',
        height: 100,
      },
      infoBox: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
      },
});