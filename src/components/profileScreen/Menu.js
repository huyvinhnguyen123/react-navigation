import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableRipple } from 'react-native-paper';
import { AuthContext } from '../../components/context';

export default function Menu({pressHandler}) {

    const { signOut } = React.useContext( AuthContext ) 

    return (
        <View style={styles.menuWrapper}>
            <TouchableRipple onPress={() => pressHandler('EditProfile')}>
              <View style={styles.menuItem}>
                <Icon name="account" color="#34cfeb" size={25}/>
                <Text style={styles.menuItemText}>Edit Account</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple onPress={() => pressHandler('EditPassword')}>
              <View style={styles.menuItem}>
                <Icon name="lock" color="#34cfeb" size={25}/>
                <Text style={styles.menuItemText}>Change Password</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple onPress={() => pressHandler('CreateGroup')}>
              <View style={styles.menuItem}>
                <Icon name="account-group" color="#34cfeb" size={25}/>
                <Text style={styles.menuItemText}>Create Group</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple onPress={() => pressHandler('Support')}>
              <View style={styles.menuItem}>
                <Icon name="account-check-outline" color="#34cfeb" size={25}/>
                <Text style={styles.menuItemText}>Support & Help</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple onPress={() => {signOut()}}>
              <View style={styles.menuItem}>
                <Icon name="logout" color="#34cfeb" size={25}/>
                <Text style={styles.menuItemText}>Sign Out</Text>
              </View>
            </TouchableRipple>
        </View>
    )
}

const styles = StyleSheet.create({
    menuWrapper: {
        marginTop: 10,
      },
      menuItem: {
        flexDirection: 'row',
        paddingVertical: 15,
        paddingHorizontal: 30,
      },
      menuItemText: {
        color: '#777777',
        marginLeft: 20,
        fontWeight: '600',
        fontSize: 16,
        lineHeight: 26,
      },
})