import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Title, Caption } from 'react-native-paper'

export default function UserInfo() {
    return (
        <View>
            <View style ={styles.userInfoSection}>
                <View style={{ flexDirection: 'row', marginTop: 50 }}>
                    <Image style={{ height: 80, width: 80, marginRight: 20, borderRadius: 50 }} source = {require('../../../image/user.jpg')} />
                    <View style={{ marginLeft: 20 }}>
                    <Title style={[styles.title],{marginTop: 15, marginBottom: 5}}> Andrea Oralie </Title>
                    <Caption style={styles.caption}> @A_Oralie </Caption>
                    </View>
                </View>
            </View>

            <View style = {styles.userInfoSection}>
                <View style={styles.row}>
                    <Icon name="map-marker-radius" color="#777777" size={20}/>
                    <Text style={{color:"#777777", marginLeft: 20}}>Florida, United State</Text>
                </View>
                <View style={styles.row}>
                    <Icon name="phone" color="#777777" size={20}/>
                    <Text style={{color:"#777777", marginLeft: 20}}>+91-900000009</Text>
                </View>
                <View style={styles.row}>
                    <Icon name="email" color="#777777" size={20}/>
                    <Text style={{color:"#777777", marginLeft: 20}}> Andrea_Oralie@email.com</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
      userInfoSection: {
        paddingHorizontal: 30,
        marginBottom: 25,
      },
      title: {
        fontSize: 24,
        fontWeight: 'bold',
      },
      caption: {
        fontSize: 14,
        lineHeight: 14,
        fontWeight: '500',
      },
      row: {
        flexDirection: 'row',
        marginBottom: 10,
      }
});