import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { AuthContext } from '../context';;

export default function EmailTextInput() {

    const { signIn } = React.useContext(AuthContext);

    return (

        <TouchableOpacity onPress={() => {signIn()}}>
            <View style={styles.signIn}>
                <Text style={[styles.textSign,{color:'#fff'}]}> Sign In </Text>
            </View>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({
    signIn: {
        width: 300,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#34cfeb",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#34cfeb',
        marginTop: 10,
    },
    textSign: {
        fontSize: 17,
        fontWeight: 'bold'
    }
})
