import React, { useState } from 'react';
import { StyleSheet, Platform, TextInput, Text, View } from 'react-native';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

export default function EmailTextInput() {

    const [data, setData] = useState({
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
    })

    const textInputChange = (val) => {
        if(val.trim().length >= 4){
          setData({
            ...data,
            email: val,
            check_textInputChange: true,
            isValidUser: true
          })
        }else{
          setData({
            ...data,
            email: val,
            check_textInputChange: false,
            isValidUser: false
          })
        }
    }

    const handleValidUser = (val) => {
        if(val.trim().length >= 4){
          setData({
            ...data,
            isValidUser: true
          })
        } else{
          setData({
            ...data,
            isValidUser: false
          })
        }
    }

    return (
        <View>
          <View style={styles.action}>
          <FontAwesome
            name="user"
            color="#05375a"
            size={20}
          />
          <TextInput
            placeholder="Your Email"
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={(val) => textInputChange(val)}
            onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
          />

            {
              data.check_textInputChange ? 
                <Animatable.View animation="bounceIn">
                  <Feather
                    name="check-circle"
                    color="green"
                    size={20}
                  />
                </Animatable.View>
              : null 
            }

          </View>

            {
              data.isValidUser ? null :
              <Animatable.View animation="fadeInLeft" duration={600}>
                <Text style={styles.errorMsg}> Username must be 4 characters long. </Text>
              </Animatable.View>
            }

        </View>
    );
}

const styles = StyleSheet.create({
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },

    textInput: {
        flex: 1,
        marginTop: Platform.And === 'ios' ? 0 : -2,
        paddingLeft: 10,
        color: '#05375a',
    },
})
