import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default function HeaderTitle({pressHandler}) {
    return (
        <View style={{flexDirection: "row", marginTop: 30 }}>
            <TouchableOpacity style={{marginLeft: '5%', marginTop: 5}} onPress={() => pressHandler()}>
                <Icon name="arrow-back" color="grey" size={20} />
            </TouchableOpacity>
                <Text style={[styles.title,{marginLeft: '23%'}]}>Create a group</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        color: '#05375a',
        fontSize: 20,
        fontWeight: '500',
        textAlign: 'center'
    },
})
