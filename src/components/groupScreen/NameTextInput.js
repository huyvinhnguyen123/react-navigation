import React from 'react'
import { StyleSheet, TouchableOpacity, View, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default function NameTextInput() {
    return (
        <View style={styles.action}>
            <TouchableOpacity style={{ padding: 10, flexDirection: 'row' }}>
                <Icon name="camera" color="grey" size={25} />
                <Icon style={{paddingTop: 2}} name="add" color="grey" size={20} />
            </TouchableOpacity>
            <TextInput placeholder="Name of group" style={[styles.input,{color:"#05375a", width: 150, borderBottomWidth: 1, borderBottomColor: '#f2f2f2'}]} />
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderRadius: 3
    },

    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        color: '#424242',
    },

    title: {
        color: '#05375a',
        fontSize: 20,
        fontWeight: '500',
        textAlign: 'center'
    },

    action: {
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: 20
        // borderBottomWidth: 1,
        // borderBottomColor: '#f2f2f2',
        // paddingBottom: 5
    },
});