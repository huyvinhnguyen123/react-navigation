import React from 'react'
import { Text, TouchableOpacity, View, Image } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';

export default function ContactSub() {
    return (
        <View style = {{ flexDirection: 'column', padding: 5, marginTop: 10 }}>
            <TouchableRipple style={{padding: 5}} onPress={() =>{}}>
                <View style = {{ flexDirection: 'row' }}>
                    <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../../image/user2.jpg')} />
                    <View style={{ flexDirection: 'column', width: '69%', marginTop: 15 }}>
                        <Text style={{ fontWeight: 'bold', }}> Andrea Oralie </Text>
                    </View>
                            {/* <TouchableOpacity style={[styles.add,{backgroundColor: 'red', borderColor:'red'}]}>
                                <Icon style={{fontWeight:'bold'}} name="remove" color="#fff" size={20} />
                            </TouchableOpacity> */}
                    <TouchableOpacity style={{justifyContent:'center'}}>
                        <Icon style={{fontWeight:'bold'}} name="remove-circle" color="red" size={35} />
                    </TouchableOpacity>
                </View>
            </TouchableRipple>
        </View>
    )
}
