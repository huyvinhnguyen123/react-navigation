import React from 'react';
import { Text, TouchableOpacity, View, Image } from 'react-native';

export default function ChatContactActive({pressHandler}) {
    return (
        <View style = {{ flexDirection: 'column', padding: 5 }}>
            <TouchableOpacity onPress={() => pressHandler()} >
                <View style = {{ flexDirection: 'row' }}>
                    <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../../image/user.jpg')} />
                    <View style={{ flexDirection: 'column', width: '69%' }}>
                        <Text style={{ fontWeight: 'bold', }}> Andrea Oralie </Text>
                        <Text style={{ color:'#000', fontWeight: 'bold', paddingTop: 3 }}> Hello i'm Andrea Oralie,... </Text>
                    </View>
                    <View style={{ flexDirection: 'column',  marginLeft: 'auto' }}>
                        <Text style = {{ textAlign:'center', fontWeight: 'bold', fontSize: 12 }}> 12:10 </Text>
                        <Text style = {{ color: '#fff', borderRadius: 50, backgroundColor: 'red', margin: 12, fontSize: 10, padding: 3 }}> 10 </Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}
