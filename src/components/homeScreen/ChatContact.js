import React from 'react';
import { Text, TouchableOpacity, View, Image } from 'react-native';

export default function ChatContactActive() {
    return (
        <View style = {{ flexDirection: 'column', padding: 5 }}>
            <View style = {{ flexDirection: 'row' }}>
                <Image style={{ height: 50, width: 50, marginRight: 20, borderRadius: 50 }} source = {require('../../../image/user2.jpg')} />
                <View style={{ flexDirection: 'column', width: '69%' }}>
                    <Text style={{color: '#777'}}> Andrea Oralie </Text>
                    <Text style={{ color:'#777', paddingTop: 3 }}> Hello i'm Andrea Oralie,... </Text>
                </View>
                <View style={{ flexDirection: 'column',  marginLeft: 'auto' }}>
                    <Text style = {{ textAlign:'center', fontSize: 12 }}> 12:10 </Text>
                </View>
            </View>
        </View>
    )
}
